from django.shortcuts import render, redirect

# from http import Http404
from django.contrib.auth.decorators import login_required
from projects.models import Project
from tasks.models import Task
from projects.forms import CreateForm

# Create your views here.


@login_required
def list_projects(request):
    list_projects = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": list_projects,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    project_object = Project.objects.get(id=id)
    try:
        task_object = Task.objects.get(id=id)
    except:
        task_object = None
    context = {
        "project_object": project_object,
        "task_object": task_object,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
